# Wordpress API widget #

Wordpress API widget is plugin for WordPress CMS. It's fetch data from 'http://192.168.99.100:3000/articles'. 

## Getting Started

To get started, you will need to install WordPress. Into ./wp-content/plugins you need to have article api and Wordpress API widget. 

## Prerequisites

For this project you will need:

```
-	WordPress
-	npm
- node-sass
```

## Installing

A step by step series of examples that tell you how to get a development env running

 - Install WordPress.
 - Put plugins into /wp-content/plugins/
 - Activate plugin over WordPress Admin 
 - Move Widget (WordPress API) to widget selection
 - Select title and how much article you want to show

## Build with

* [PHP](https://php.net/) - Programming language 
* [WordPress](https://codex.wordpress.org/) - CMS build in PHP

