<?php   
    // Template for number of posts in Admin panel
?>
<p>
    <label for="<?php echo $this->get_field_id('num_posts'); ?>"><?php _e('Number of posts:'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('num_posts'); ?>" name="<?php echo $this->get_field_name('num_posts'); ?>" type="text" value="<?php echo esc_attr($num_posts); ?>" />
</p>