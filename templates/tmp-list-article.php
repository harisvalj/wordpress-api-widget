<?php
    // Display article into widget front
?>
<div class="article">
    <h4><?php _e($title); ?></h4>
    <div class="article-content"><?php _e($content); ?></div>
</div>
