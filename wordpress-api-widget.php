<?php

/**
 * Plugin Name: Wordpress API widget
 * Description: Fetches articles from the article API.
 * Version: 0.0.1
 * Author: 
 */

define('WORDPRESS_API_WIDGET_PATH', plugin_dir_path(__FILE__));
define('WORDPRESS_API_WIDGET_URI', plugins_url('', __FILE__));

// Import style into plugin

function add_my_stylesheet() 
{
    wp_enqueue_style( 'myCSS', plugins_url( '/style.css', __FILE__ ) );
}

add_action('init', 'add_my_stylesheet');

// Widget registration and load

function load_article_list()
{
    register_widget('ArticleList');
}

add_action('widgets_init', 'load_article_list');

class ArticleList extends WP_Widget
{
    // class constructor

    private $articles;

    public function __construct()
    {
        parent::__construct(

            // Base ID of your widget
            'article_list',

            // Widget name will appear in UI
            __('Article list', 'article_list_domain'),

            // Widget description
            array('description' => __('Article listing', 'article_list_domain'),)
        );

        // Fetch data from API
        $getData = file_get_contents('http://192.168.99.100:3000/articles');
        if(!empty($getData)){
            $this->articles = json_decode($getData);
        }else{
            $this->articles = [];
        }
        

    }

    // output the widget content on the front-end
    public function widget($args, $instance)
    {
        
        // Get and display Widget title
        $title = apply_filters('widget_title', $instance['title']);        
        
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];
        
        // Get number of max posts
        $num_posts = $instance['num_posts'];
        
        // Number of min posts
        $i = 0;

        // Loop for display posts
        foreach($this->articles as $article){
            if($i == $num_posts){
                break;
            }else{
                $this->displayArticle($article);
                $i++;
            }
        }
       

    }

    public function displayArticle($data){
        /*
            Get Data from foreach loop and make 
        */
        $id = $data->id;
        $title = $data->title;
        $content = $data->content;
        include(plugin_dir_path(__FILE__) . 'templates/tmp-list-article.php');
    }

    // output the option form field in admin Widgets screen
    public function form($instance)
    {

        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Article list', 'article_list_domain');
        }

        if (isset($instance['num_posts'])) {
            $num_posts = $instance['num_posts'];
        } else {
            $num_posts = 1;
        }
        // 
        include(plugin_dir_path(__FILE__) . 'templates/tmp-widget-title.php');
        include(plugin_dir_path(__FILE__) . 'templates/tmp-widget-num-posts.php');
    }

    // save options
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['num_posts'] = (!empty($new_instance['num_posts'])) ? $new_instance['num_posts'] : '1';
        return $instance;
    }
}
